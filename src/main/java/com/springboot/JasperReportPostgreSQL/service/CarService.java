package com.springboot.JasperReportPostgreSQL.service;

import com.springboot.JasperReportPostgreSQL.entity.Car;
import com.springboot.JasperReportPostgreSQL.repository.CarRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    private String getCurrentDate() {
        String pattern = "dd-MM-yyyy_HH-mm-ss";
        DateFormat df = new SimpleDateFormat(pattern);

        return df.format(new Date());
    }

    public String exportReport(String reportFormat, String template) throws JRException {


        // http://localhost:8080/report?format=pdf&template=cars
        String path = "C:\\Users\\kuzma_m\\IdeaProjects\\JasperReportPostgreSQL\\src\\main\\resources\\" + template + ".jrxml";

        List<Car> cars = carRepository.findAll();

        JasperReport jasperReport = JasperCompileManager.compileReport(path);

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cars);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("parameter", "Car list");

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, "C:\\Users\\kuzma_m\\" +
                    "SpringBootData\\CarReport\\" + getCurrentDate() + ".html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\Users\\kuzma_m\\" +
                    "SpringBootData\\CarReport\\" + getCurrentDate() + ".pdf");
        }
        return "report generated in path : " + path;
    }
}
