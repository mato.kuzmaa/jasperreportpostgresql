package com.springboot.JasperReportPostgreSQL.controller;

import com.springboot.JasperReportPostgreSQL.entity.Car;
import com.springboot.JasperReportPostgreSQL.repository.CarRepository;
import com.springboot.JasperReportPostgreSQL.service.CarService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {

    private final CarService carService;
    private final CarRepository carRepository;

    public CarController(CarService carService,
                         CarRepository carRepository) {
        this.carService = carService;
        this.carRepository = carRepository;
    }

    @GetMapping("/getCars")
    public List<Car> getCars() {
        return carRepository.findAll();
    }


    // http://localhost:8085/report?format=pdf&template=green
    @GetMapping("/report")
    public String generateReport(@RequestParam String format, @RequestParam String template)
            throws JRException {
        return carService.exportReport(format, template);
    }
}
