package com.springboot.JasperReportPostgreSQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasperReportPostgreSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasperReportPostgreSqlApplication.class, args);
	}

}
