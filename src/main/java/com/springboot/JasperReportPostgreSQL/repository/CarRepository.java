package com.springboot.JasperReportPostgreSQL.repository;

import com.springboot.JasperReportPostgreSQL.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}
