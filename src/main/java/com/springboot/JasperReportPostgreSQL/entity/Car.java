package com.springboot.JasperReportPostgreSQL.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
@Entity
@Table(name = "cars")
public class Car {

    @Id
    private Integer id;
    private String brand;
    private String model;
    private String color;
    private Integer price;
}
